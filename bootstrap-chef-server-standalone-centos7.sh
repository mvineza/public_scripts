#!/bin/bash
RPM_VERSION="13.1.13"
RPM_FILE="chef-server-core-${RPM_VERSION}-1.el7.x86_64.rpm"
rpm -ivh https://packages.chef.io/files/stable/chef-server/${RPM_VERSION}/el/7/${RPM_FILE}
# experiencing slowness in downloading RPM file so download on host and transfer to guest for the meanwhile 
# rpm -ivh /tmp/${RPM_FILE}
echo yes | chef-server-ctl reconfigure
chef-server-ctl user-create admin Chef Administrator admin@nxdomain.com 'pass123' --filename admin.pem
chef-server-ctl org-create sample 'My Sample Organization' --association_user admin --filename sample-validator.pem
echo yes | chef-server-ctl install chef-manage
echo yes | chef-server-ctl reconfigure
chef-manage-ctl reconfigure --accept-license